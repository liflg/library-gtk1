#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        CONFIGUREOPTIONS="--enable-debug=yes"
    else
        CONFIGUREOPTIONS="--enable-debug=no"
    fi

    if [[ ( -z "$OPTIMIZATION" ) || ( "$OPTIMIZATION" -eq 2 ) ]]; then
        export CFLAGS="$CFLAGS -O2"
    else
        echo 'Optimization level '$OPTIMIZATION' is not yet implemented!'
        exit 1
    fi

    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"

cat << EOF > $BUILDDIR/glib-config
#!/bin/sh

prefix=${PWD}/../library-glib1/${MULTIARCHNAME}
exec_prefix=\${prefix}

lib_glib=yes

while test \$# -gt 0; do
  case "\$1" in
  -*=*) optarg=\`echo "\$1" | sed 's/[-_a-zA-Z0-9]*=//'\` ;;
  *) optarg= ;;
  esac

  case \$1 in
    --version)
      echo 1.2.10
      exit 0
      ;;
    --cflags)
      echo_cflags=yes
      ;;
    --libs)
      echo_libs=yes
      ;;
    glib)
      lib_glib=yes
      ;;
    gmodule)
      lib_gmodule=yes
      ;;
    gthread)
      lib_gthread=yes
      ;;
  esac
  shift
done

if test "\$echo_cflags" = "yes"; then
    cflags=""
    if test "\$lib_gthread" = "yes"; then
        cflags="\$cflags  -D_REENTRANT"
    fi
    echo -I\${prefix}/include/glib-1.2 -I\${exec_prefix}/lib/glib/include \$includes \$cflags
fi
if test "\$echo_libs" = "yes"; then
    libsp=""
    libsa=""
    if test "\$lib_glib" = "yes"; then
        libsp="\$libsp -lglib"
    fi
    if test "\$lib_gthread" = "yes"; then
        libsp="-lgthread \$libsp"
        libsa="\$libsa -lpthread"
    fi
    if test "\$lib_gmodule" = "yes"; then
        libsp="-rdynamic -lgmodule \$libsp"
        libsa="\$libsa -ldl"
    fi
    echo -L\${exec_prefix}/lib \$libsp \$libsa
fi
EOF

    chmod 755 "$BUILDDIR"/glib-config
    export GLIB_CONFIG="$BUILDDIR"/glib-config
    export LD_LIBRARY_PATH=${PWD}/../library-glib1/${MULTIARCHNAME}/lib

    ( cd "$BUILDDIR"
      ../source/configure \
         --prefix="$PREFIXDIR" \
         --disable-static \
         --disable-nls \
         $CONFIGUREOPTIONS
      make -j $(nproc)
      make install
      rm -rf "$PREFIXDIR"/{bin,etc,info,lib/*.la,lib/pkgconfig,man,share})

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout . )
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="$PWD/build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/COPYING "$PREFIXDIR"/lib/LICENSE-gtk1.txt

rm -rf "$BUILDDIR"

echo "Gtk1 for $MULTIARCHNAME is ready."
