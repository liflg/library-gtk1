Website
=======
http://www.gtk.org/

License
=======
GNU GPL version 2 (see the file source/COPYING)

Version
=======
1.2.10

Source
======
gtk+-1.2.10.tar.gz (sha256: 3fb843ea671c89b909fd145fa09fd2276af3312e58cbab29ed1c93b462108c34)

Requires
========
* Glib1